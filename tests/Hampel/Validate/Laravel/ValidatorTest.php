<?php namespace Hampel\Validate\Laravel;
/**
 * 
 */

use Mockery;
use App, Validator;
use Hampel\Validate\Laravel\Validator as ValidatorClass;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ValidatorTest extends \Orchestra\Testbench\TestCase
{

	protected function getPackageProviders()
	{
		return array(
			'Hampel\Validate\Laravel\ValidateServiceProvider',
			'Illuminate\Validation\ValidationServiceProvider',
		);
	}

	protected function getPackageAliases ()
	{
		return array(
			'Validator' => 'Illuminate\Support\Facades\Validator',
		);

	}

	public function setUp()
	{
		parent::setUp();

		$this->translator = Mockery::mock('Symfony\Component\Translation\TranslatorInterface');

		$this->validator = Mockery::mock('Hampel\Validate\Validator');

		app()['validate-laravel.validator'] = $this->validator;

		$this->tldcache = Mockery::mock('Hampel\Validate\Laravel\TldCache');

		app()['validate-laravel.tlds'] = $this->tldcache;
	}


	public function testValidateUniqueOrZero()
	{
		$v = new ValidatorClass($this->translator, array('foo' => 0), array('foo' => 'unique_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->never();
		$v->setPresenceVerifier($mock);
		$this->assertTrue($v->passes());

		$v = new ValidatorClass($this->translator, array('foo' => 1), array('foo' => 'unique_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->once()->with('bar', 'foo', 1, null, null, array())->andReturn(0);
		$v->setPresenceVerifier($mock);
		$this->assertTrue($v->passes());

		$this->translator->shouldReceive('trans');

		$v = new ValidatorClass($this->translator, array('foo' => 1), array('foo' => 'unique_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->once()->with('bar', 'foo', 1, null, null, array())->andReturn(1);
		$v->setPresenceVerifier($mock);
		$this->assertFalse($v->passes());
	}

	public function testValidateExistsOrZero()
	{
		$v = new ValidatorClass($this->translator, array('foo' => 0), array('foo' => 'exists_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->never();
		$v->setPresenceVerifier($mock);
		$this->assertTrue($v->passes());

		$v = new ValidatorClass($this->translator, array('foo' => 1), array('foo' => 'exists_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->once()->with('bar', 'foo', 1, null, null, array())->andReturn(1);
		$v->setPresenceVerifier($mock);
		$this->assertTrue($v->passes());

		$this->translator->shouldReceive('trans');

		$v = new ValidatorClass($this->translator, array('foo' => 1), array('foo' => 'exists_or_zero:bar'));
		$mock = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$mock->shouldReceive('getCount')->once()->with('bar', 'foo', 1, null, null, array())->andReturn(0);
		$v->setPresenceVerifier($mock);
		$this->assertFalse($v->passes());
	}

	public function testValidateBool()
	{
		$this->validator->shouldReceive('isBool')->once()->with(0)->andReturn(true);
		$v = new ValidatorClass($this->translator, array('foo' => 0), array('foo' => 'bool'));
		$this->assertTrue($v->passes());

		$this->validator->shouldReceive('isBool')->once()->with('no')->andReturn(true);
		$v = new ValidatorClass($this->translator, array('foo' => 'no'), array('foo' => 'bool'));
		$this->assertTrue($v->passes());

		$this->translator->shouldReceive('trans');

		$this->validator->shouldReceive('isBool')->once()->with('bar')->andReturn(false);
		$v = new ValidatorClass($this->translator, array('foo' => 'bar'), array('foo' => 'bool'));
		$this->assertFalse($v->passes());
	}

	public function testValidateDomainIn()
	{
		$this->validator->shouldReceive('isDomain')->once()->with('bar.com', array('com', 'net', 'org'))->andReturn(true);
		$v = new ValidatorClass($this->translator, array('foo' => 'bar.com'), array('foo' => 'domain_in:com,net,org'));
		$this->assertTrue($v->passes());

		$this->translator->shouldReceive('trans')->once()->with('validation.custom.foo.domain_in')->andReturn(':attribute TLD must be one of :values');
		$this->translator->shouldReceive('trans')->once()->with('validation.attributes.foo')->andReturn('foo');

		$this->validator->shouldReceive('isDomain')->once()->with('bar.biz', array('com', 'net', 'org'))->andReturn(false);
		$v = new ValidatorClass($this->translator, array('foo' => 'bar.biz'), array('foo' => 'domain_in:com,net,org'));
		$this->assertFalse($v->passes());

		$this->assertEquals($v->errors()->first('foo'), 'foo TLD must be one of com, net, org');
	}

	public function testValidateTLD()
	{
		$this->tldcache->shouldReceive('getTLDs')->once()->andReturn(array('com', 'net', 'org'));

		$this->validator->shouldReceive('isTLD')->once()->with('bar.com', array('com', 'net', 'org'))->andReturn(true);
		$v = new ValidatorClass($this->translator, array('foo' => 'bar.com'), array('foo' => 'tld'));
		$this->assertTrue($v->passes());

		$this->translator->shouldReceive('trans');
		$this->tldcache->shouldReceive('getTLDs')->once()->andReturn(array('com', 'net', 'org'));

		$this->validator->shouldReceive('isTLD')->once()->with('bar.biz', array('com', 'net', 'org'))->andReturn(false);
		$v = new ValidatorClass($this->translator, array('foo' => 'bar.biz'), array('foo' => 'tld'));
		$this->assertFalse($v->passes());
	}

	public function testIsValid()
	{
		$file = new UploadedFile(
			__DIR__.'/Fixtures/test.gif',
			'original.gif',
			null,
			filesize(__DIR__.'/Fixtures/test.gif'),
			UPLOAD_ERR_OK,
			true
		);

		$this->assertTrue($file->isValid());

		$v = new ValidatorClass($this->translator, array('file' => $file), array('file' => 'uploaded_file'));

		$this->assertTrue($v->passes());
	}

	/**
	 * @dataProvider uploadedFileErrorProvider
	 */
	public function testIsInvalidOnUploadError($error)
	{
		$file = new UploadedFile(
			__DIR__.'/Fixtures/test.gif',
			'original.gif',
			null,
			filesize(__DIR__.'/Fixtures/test.gif'),
			$error
		);

		$this->assertFalse($file->isValid());

		$v = new ValidatorClass($this->translator, array('file' => $file), array('file' => 'uploaded_file'));

		$this->translator->shouldReceive('trans')->once()->with('validation.custom.file.uploaded_file')->andReturn(':attribute upload did not succeed');
		$this->translator->shouldReceive('trans')->once()->with('validation.attributes.file')->andReturn('file');

		$this->assertTrue($v->fails());

		$this->assertEquals($v->messages()->first('file'), 'file upload did not succeed');
	}

	public function uploadedFileErrorProvider()
	{
		return array(
			array(UPLOAD_ERR_INI_SIZE),
			array(UPLOAD_ERR_FORM_SIZE),
			array(UPLOAD_ERR_PARTIAL),
			array(UPLOAD_ERR_NO_TMP_DIR),
			array(UPLOAD_ERR_EXTENSION),
		);
	}

	public function tearDown() {
		Mockery::close();
	}


}

?>
