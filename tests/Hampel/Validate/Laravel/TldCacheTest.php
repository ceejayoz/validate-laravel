<?php namespace Hampel\Validate\Laravel;
/**
 * 
 */

use Mockery;
use App;

class TldCacheTest extends \Orchestra\Testbench\TestCase
{
	protected function getPackageProviders()
	{
		return array(
			'Hampel\Validate\Laravel\ValidateServiceProvider'
		);
	}

	public function setUp()
	{
		parent::setUp();

		$this->app = Mockery::mock('AppMock');
		$this->app->shouldReceive('instance')->andReturn($this->app);

		\Illuminate\Support\Facades\Facade::setFacadeApplication($this->app);

		$this->config = Mockery::mock('ConfigMock');
		\Illuminate\Support\Facades\Config::swap($this->config);

		$this->cache = Mockery::mock('CacheMock');
		\Illuminate\Support\Facades\Cache::swap($this->cache);

		$this->log = Mockery::mock('LogMock');
		\Illuminate\Support\Facades\Log::swap($this->log);

		$this->file = Mockery::mock('Illuminate\Filesystem\Filesystem');
		\Illuminate\Support\Facades\File::swap($this->file);

	}

	public function testContructTldCache()
	{
		$tldcache = new TldCache($this->file);

		$this->assertInstanceOf('Hampel\Validate\Laravel\TldCache', $tldcache);
	}

	public function testGetTLDs()
	{
		$this->config->shouldReceive('get')
			->once()
			->with('validate-laravel::cache_key')
			->andReturn('tlds');

		$this->config->shouldReceive('get')
			->once()
			->with('validate-laravel::cache_expiry')
			->andReturn(1440);

		$this->cache->shouldReceive('remember')
			->once()
			->with('tlds', 1440, Mockery::type('Closure'))
			->andReturn(array('com', 'net', 'org'));

		$tldcache = new TldCache($this->file);
		$tlds = $tldcache->getTLDs();

		$this->assertTrue(is_array($tlds));
		$this->assertTrue(count($tlds) == 3);
		$this->assertTrue($tlds[0] == 'com');
	}

	public function testRefreshTLDCache()
	{
		$tlds = file_get_contents(__DIR__ . '/Fixtures/tlds-alpha-by-domain.txt');

		$this->config->shouldReceive('get')
			->once()
			->with('validate-laravel::tld_path')
			->andReturn('http://data.iana.org/TLD/tlds-alpha-by-domain.txt');

		$this->file->shouldReceive('getRemote')
			->once()
			->with('http://data.iana.org/TLD/tlds-alpha-by-domain.txt')
			->andReturn($tlds);

		$this->log->shouldReceive('info')
			->once()
			->with('Added 3 TLDs to cache from http://data.iana.org/TLD/tlds-alpha-by-domain.txt');

		$tldcache = new TldCache($this->file);
		$tlds = $tldcache->refreshTLDCache();

		$this->assertTrue(is_array($tlds));
		$this->assertTrue(count($tlds) == 3);
		$this->assertTrue($tlds[0] == 'com');
	}

	public function testRefreshTLDCacheGetFailes()
	{
		$this->config->shouldReceive('get')
			->once()
			->with('validate-laravel::tld_path')
			->andReturn('http://data.iana.org/TLD/tlds-alpha-by-domain.txt');

		$this->file->shouldReceive('getRemote')
			->once()
			->with('http://data.iana.org/TLD/tlds-alpha-by-domain.txt')
			->andReturn(false);

		$tldcache = new TldCache($this->file);
		$tlds = $tldcache->refreshTLDCache();

		$this->assertTrue(is_array($tlds));
		$this->assertTrue(empty($tlds));
	}

	public function tearDown() {
		Mockery::close();
	}

}

?>
