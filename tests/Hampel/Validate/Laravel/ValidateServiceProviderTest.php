<?php namespace Hampel\Validate\Laravel;

use App, Validator;

class ValidateServiceProviderTest extends \Orchestra\Testbench\TestCase {

	protected function getPackageProviders()
	{
		return array('Hampel\Validate\Laravel\ValidateServiceProvider');
	}

	public function testMakeServices()
	{
		$this->assertInstanceOf('Hampel\Validate\Validator', App::make('validate-laravel.validator'));

		$this->assertInstanceOf('Hampel\Validate\Laravel\TldCache', App::make('validate-laravel.tlds'));

		$v = Validator::make(array(), array());
		$extensions = $v->getExtensions();

		$this->assertArrayHasKey('unique_or_zero', $extensions);
		$this->assertArrayHasKey('exists_or_zero', $extensions);
		$this->assertArrayHasKey('bool', $extensions);
		$this->assertArrayHasKey('ipv4_public', $extensions);
		$this->assertArrayHasKey('ipv6_public', $extensions);
		$this->assertArrayHasKey('ip_public', $extensions);
		$this->assertArrayHasKey('domain', $extensions);
		$this->assertArrayHasKey('domain_in', $extensions);
		$this->assertArrayHasKey('tld', $extensions);
		$this->assertArrayHasKey('tld_in', $extensions);
		$this->assertArrayHasKey('uploaded_file', $extensions);
	}
}

?>