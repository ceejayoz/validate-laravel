<?php namespace Hampel\Validate\Laravel;

use Illuminate\Support\ServiceProvider;
use Hampel\Validate\Validator as ValidatorClass;

class ValidateServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('hampel/validate-laravel', 'validate-laravel', __DIR__ . '/../../..');

		$app = $this->app;

		$this->app->bind('Hampel\Validate\Laravel\Validator', function($app)
		{
			return new Validator($app['translator'], array(), array(), $app['translator']->get('validate-laravel::validation'));
		});

		$validatorNamespace = 'Hampel\Validate\Laravel\\';

		$translation = $this->app['translator']->get('validate-laravel::validation');

		$this->app['validator']->extend('unique_or_zero', $validatorNamespace . 'Validator@validateUniqueOrZero', $translation['unique_or_zero']);
		$this->app['validator']->extend('exists_or_zero', $validatorNamespace . 'Validator@validateExistsOrZero', $translation['exists_or_zero']);
		$this->app['validator']->extend('bool', $validatorNamespace . 'Validator@validateBool', $translation['bool']);
		$this->app['validator']->extend('ipv4_public', $validatorNamespace . 'Validator@validateIpv4Public', $translation['ipv4_public']);
		$this->app['validator']->extend('ipv6_public', $validatorNamespace . 'Validator@validateIpv6Public', $translation['ipv6_public']);
		$this->app['validator']->extend('ip_public', $validatorNamespace . 'Validator@validateIpPublic', $translation['ip_public']);
		$this->app['validator']->extend('domain', $validatorNamespace . 'Validator@validateDomain', $translation['domain']);
		$this->app['validator']->extend('domain_in', $validatorNamespace . 'Validator@validateDomainIn', $translation['domain_in']);
		$this->app['validator']->extend('tld', $validatorNamespace . 'Validator@validateTLD', $translation['tld']);
		$this->app['validator']->extend('tld_in', $validatorNamespace . 'Validator@validateTLDIn', $translation['tld_in']);
		$this->app['validator']->extend('uploaded_file', $validatorNamespace . 'Validator@validateUploadedFile', $translation['uploaded_file']);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$app = $this->app;

		$this->app['validate-laravel.tlds'] = $this->app->share(function() use ($app)
		{
			return new TldCache($app['files']);
		});

		$this->app['validate-laravel.validator'] = $this->app->share(function()
		{
			return new ValidatorClass();
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('validate-laravel.tlds', 'validate-laravel.validator');
	}

}