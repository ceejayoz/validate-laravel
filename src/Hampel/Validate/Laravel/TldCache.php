<?php namespace Hampel\Validate\Laravel;
/**
 * 
 */

use Log, Cache, Config;
use Illuminate\Filesystem\Filesystem;

class TldCache
{
	protected $filesystem;

	public function __construct(Filesystem $filesystem)
	{
		$this->filesystem = $filesystem;
	}

	public function getTLDs()
	{
		$tlds = Cache::remember(Config::get('validate-laravel::cache_key'), Config::get('validate-laravel::cache_expiry'), function()
		{
			return $this->refreshTLDCache();
		});

		return $tlds;
	}

	public function refreshTLDCache()
	{
		$tlds = array();
		$tld_file = false;

		$tld_path = Config::get('validate-laravel::tld_path');

		if (empty($tld_path)) $tld_path = __DIR__ . '/tlds-alpha-by-domain.txt'; // use local file if blank path specified in config

		$tld_file = $this->filesystem->getRemote($tld_path);
		if ($tld_file === false) return $tlds; // return an empty array on failure

		$tld_array = explode("\n", $tld_file);
		foreach ($tld_array as $tld)
		{
			$tld = trim($tld);
			if (empty($tld)) continue; // skip blank lines
			if (substr($tld, 0, 1) == "#") continue; // skip # comments

			$tlds[] = strtolower($tld);
		}

		Log::info("Added " . count($tlds) . " TLDs to cache from {$tld_path}");
		return $tlds;
	}
}

?>
