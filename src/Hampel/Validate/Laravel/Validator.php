<?php namespace Hampel\Validate\Laravel;
/**
 * 
 */

use App;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Validator extends \Illuminate\Validation\Validator
{
	public function validateUniqueOrZero($attribute, $value, $parameters)
	{
		if ($value == 0) return true;

		return $this->validateUnique($attribute, $value, $parameters);
	}

	public function validateExistsOrZero($attribute, $value, $parameters)
	{
		if ($value == 0) return true;

		return $this->validateExists($attribute, $value, $parameters);
	}

	public function validateBool($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isBool($value);
	}

	public function validateIpv4Public($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isPublicIPv4($value);
	}

	public function validateIpv6Public($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isPublicIPv6($value);
	}

	public function validateIpPublic($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isPublicIP($value);
	}

	public function validateDomain($attribute, $value, $parameters)
	{
		$tlds = App::make('validate-laravel.tlds')->getTLDs();

		$validator = App::make('validate-laravel.validator');

		return $validator->isDomain($value, $tlds);
	}

	public function validateDomainIn($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isDomain($value, $parameters);
	}

	protected function replaceDomainIn($message, $attribute, $rule, $parameters)
	{
		return str_replace(':values', implode(', ', $parameters), $message);
	}

	public function validateTLD($attribute, $value, $parameters)
	{
		$tlds = App::make('validate-laravel.tlds')->getTLDs();

		$validator = App::make('validate-laravel.validator');

		return $validator->isTLD($value, $tlds);
	}

	public function validateTLDIn($attribute, $value, $parameters)
	{
		$validator = App::make('validate-laravel.validator');

		return $validator->isTLD($value, $parameters);
	}

	protected function replaceTLDIn($message, $attribute, $rule, $parameters)
	{
		return str_replace(':values', implode(', ', $parameters), $message);
	}

	protected function validateUploadedFile($attribute, $value, $parameters)
	{
		return ($value instanceof UploadedFile AND $value->isValid());
	}
}

?>
