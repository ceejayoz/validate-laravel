<?php
/**
 * Configuration for Validator
 */

return array(

	/*
	|--------------------------------------------------------------------------
	| TLD Path
	|--------------------------------------------------------------------------
	|
	| Specify the path or URL to retrieve a list of TLDs for use in domain/TLD
	| verification.
	|
	| Path will be passed to file_get_contents, so any valid path or URL may be
	| used.
	|
	| File format is assumed to be:
	| - one TLD per line
	| - either upper or lower case
	| - lines beginning with # are ignored
	| - internationalized domains allowed using punycode notation
	|
	| Set to empty string '', to force usage of local file included in package
	|
	*/

	'tld_path' => 'http://data.iana.org/TLD/tlds-alpha-by-domain.txt',

	/*
	|--------------------------------------------------------------------------
	| Cache Expiry
	|--------------------------------------------------------------------------
	|
	| How long should the application cache TLD data - in minutes
	| Default: 1440 minutes = 1 day
	|
	*/

	'cache_expiry' => 1440,

	/*
	|--------------------------------------------------------------------------
	| Cache Keys
	|--------------------------------------------------------------------------
	|
	| Key to cache TLD information, only need to change this in the case of
	| conflicts with other packages or code
	|
	*/

	'cache_key' => 'tlds',

);

?>
